// Given a list of integers, return the largest product that can be made by multiplying any three integers.
// For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.
// You can assume the list has at least three integers.

// Solution
// If all the integers were positive, then we would simply need to take the three largest numbers of the array. Then, we can just sort it and return the last three elements.

// However, we need to account for negative numbers in the array. If the largest product that can be made includes a negative number, we would need to have two
//  so as to cancel out the negatives. So, we can take the larger of:

// The three largest numbers
// The two smallest (most negative) numbers, and the largest number


const maxProductOfThree = {
    v1: function (lst) {
        lst.sort((a, b) => a - b);
        let third_largest = lst[lst.length - 3];
        let second_largest = lst[lst.length - 2];
        let first_largest = lst[lst.length - 1];
        let first_smallest = lst[0];
        let second_smallest = lst[1];

        return Math.max(third_largest * second_largest * first_largest, first_largest * first_smallest * second_smallest);
    },

    v2: function (lst) {
        let max1 = -Infinity;
        let max2 = -Infinity;
        let max3 = -Infinity;
        let min1 = Infinity;
        let min2 = Infinity;

        for (let x of lst) {
            if (x > max1) {
                max3 = max2;
                max2 = max1;
                max1 = x;
            } else if (x > max2) {
                max3 = max2;
                max2 = x;
            } else if (x > max3) {
                max3 = x;
            }

            if (x < min1) {
                min2 = min1;
                min1 = x;
            } else if (x < min2) {
                min2 = x;
            }
        }

        return Math.max(max1 * max2 * max3, max1 * min1 * min2);
    }
}






const list = [-10, 5, 2, -10];
console.log(maxProductOfThree.v1(list));