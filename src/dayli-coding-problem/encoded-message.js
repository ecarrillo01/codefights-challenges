//https://www.dailycodingproblem.com/solution/7?token=5ab29df6a6871e4c6eecfdc5843e22ea622634caf7ec49f633ffeb0f969207250dcfa90f
//Difficulty: medium
function num_encodings(s) {
    const cache = new Map();
    cache.set(s.length, 1);
    for (let i = s.length - 1; i >= 0; i--) {
        if (s[i].startsWith('0')) {
            cache.set(i, 0);
        } else if (i === s.length - 1) {
            cache.set(i, 1);
        } else {
            if (parseInt(s.slice(i, i + 2)) <= 26) {
                cache.set(i, cache.get(i + 2));
            }
            cache.set(i, cache.get(i) + cache.get(i + 1));
        }
    }
    return cache.get(0);
}

console.log(num_encodings('111'))
