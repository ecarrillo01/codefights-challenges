// Given a string of parentheses, write a function to compute the minimum number of parentheses to be removed to make the string valid (i.e. each open parenthesis is eventually closed).
// For example, given the string "()())()", you should return 1. Given the string ")(", you should return 2, since we must remove all of them.

function solve(s) {
    let total = 0
    let temp = 0
    for (let p of s) {
        if (p == "(") {
            total += 1;
        }
        else if (p == ")" && total) {
            total -= 1
        }
        else {
            temp += 1
        }
    }
    return total + temp
}

console.log(solve(")("));