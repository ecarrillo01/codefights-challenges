//https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/lecture/11183944#overview
// Count unique values in a sorted array
function countUniqueValues(arr) {
    // add whatever parameters you deem necessary - good luck!
    if (!arr.length) return 0;
    let i = 0;
    for (let j = 1; j < arr.length; j++) {
        if (arr[i] !== arr[j]) {
            i++;
            arr[i] = arr[j];
        }
    }
    return i + 1;

}

module.exports = countUniqueValues;