const isLeapYear = (year) =>
    (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;


function dayOfProgrammer(year) {
    if (year === 1918) return '26.09.1918';
    return `${year % 4 === 0 && year < 1918 || isLeapYear(year) ? '12' : '13'}.09.${year}`;
}

console.log(dayOfProgrammer(2026));