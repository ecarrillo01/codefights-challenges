const { isLeapYear } = require("./utils/utils");

/**
 * 
 * @param {*} date 2024/03/16
 * @returns number
 */
function calculateDayNumber(date) {
    const [year, month, day] = date.split('/');
    let daysSum = parseInt(day);
    const monthDays = [31, isLeapYear(parseInt(year)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    for (let i = 0; i < parseInt(month) - 1; i++) {
        daysSum += monthDays[i];
    }

    return daysSum;

}

console.log(calculateDayNumber('2024/10/12'));