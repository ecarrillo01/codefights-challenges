// https://www.hackerrank.com/challenges/ctci-making-anagrams/problem?isFullScreen=true&h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
// See: https://www.youtube.com/watch?v=9wAbly6lcj8
function anagrams(a, b) {
    const strArr1 = a.split("");
    const strArr2 = b.split("");
    let ob = {};
    let cnt = 0;
    for (let item of strArr1) {
        ob[item] = ++ob[item] || 1;
    }

    for (let item of strArr2) {
        if (ob[item]) {
            ob[item]--;
        } else {
            cnt++;
        }
    }

    for (let key in ob) {
        cnt = cnt + ob[key]
    }

    return cnt;

}

console.log(anagrams("fcrxzwscanmligyxyvym", "jxwtrhvujlmrpdoqbisbwhmgpmeoke"));