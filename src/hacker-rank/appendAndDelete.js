// https://www.hackerrank.com/challenges/append-and-delete/problem
function appendAndDelete(s, t, k) {
    let commonIndex = 0;
    if (s.length + t.length < k) {
        return 'Yes';
    }
    for (let i = 0; i < s.length; i++) {
        let s1 = s[i];
        let s2 = t[i];
        if (s1 === s2) {
            commonIndex++;
        }

        else {
            break;
        }
    }

    if (commonIndex === s.length && s.length === t.length) {
        if (k % 2 === 0) return 'Yes';
        return 'No';
    }

    // Reduce k by number of necessary deletions and insertions
    k = k - (s.length - commonIndex) - (t.length - commonIndex);

    // If k < 0 or there is an odd number of operations left over, false
    // else we need exactly k operations or the number of extra ops is even, true
    return (k < 0 || (k & 1) == 1) ? 'No' : 'Yes';
}


// console.log(appendAndDelete('y', 'yu', 2)); //no
// console.log(appendAndDelete('zzzzz', 'zzzzzzz', 4));//yes
// console.log(appendAndDelete('hackerhappy', 'hackerrank', 9));//yes
// console.log(appendAndDelete('aba', 'aba', 7));//yes
// console.log(appendAndDelete('aaaaaaaaaa', 'aaaaa', 7));//yes
console.log(appendAndDelete('asdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcv', 'asdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcvasdfqwertyuighjkzxcv', 20))
