//https://www.hackerrank.com/challenges/beautiful-days-at-the-movies/problem

const reversedNum = num => parseFloat(num.toString().split('').reverse().join('')) * Math.sign(num);
function beautifulDays(i, j, k) {
    let cnt = 0;
    while (i <= j) {
        const exp = (i - reversedNum(i)) % k;
        if (exp === 0) {
            cnt++
        }
        i++;
    }

    return cnt;
}

console.log(beautifulDays(20, 23, 6));