// https://www.hackerrank.com/challenges/cats-and-a-mouse/problem?h_r=next-challenge&h_v=zen
function catAndMouse(x, y, z) {
    const xLen = Math.abs(x - z);
    const yLen = Math.abs(y - z);
    if (xLen < yLen) {
        return 'Cat A';
    }

    if (xLen > yLen) {
        return 'Cat B';
    }

    return 'Mouse C ';

}