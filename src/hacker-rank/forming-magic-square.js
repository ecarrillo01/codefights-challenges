const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);
function reverse3dMatrix(arr) {
    return arr.map(item => [...item].reverse());
}

function rotateMatrix(arr) {
    let arr1 = [];
    for (let i = 0; i < arr.length; i++) {
        arr1[i] = [];
        for (let j = 0; j < arr[i].length; j++) {
            arr1[i][j] = 0;
        }
    }
    for (let i = 0, max = arr.length; i < max; i++) {
        let current = arr[i];
        for (let j = 0, max1 = current.length; j < max1; j++) {
            let key1 = max1 - 1 - j;
            arr1[i][j] = arr[key1][i];
        }
    }
    return arr1;
}
// const magicSquares = (() => {
//     let squares = [
//         [
//             [8, 1, 6],
//             [3, 5, 7],
//             [4, 9, 2]
//         ]
//     ];

//     //1
//     squares.push(reverse3dMatrix(squares[0]));
//     //2
//     squares.push(pipe(rotateMatrix, rotateMatrix, reverse3dMatrix)(squares[0]));
//     //3
//     squares.push(reverse3dMatrix(squares[2]));
//     //4
//     squares.push(pipe(rotateMatrix, reverse3dMatrix)(squares[0]));
//     //5
//     squares.push(rotateMatrix(squares[0]));
//     //6
//     squares.push(pipe(rotateMatrix, reverse3dMatrix)(squares[1]));
//     //7
//     squares.push(rotateMatrix(squares[1]));

//     return squares;
// })();

const magicSquares = [
    [
        [8, 1, 6],
        [3, 5, 7],
        [4, 9, 2]
    ],
    [
        [6, 1, 8],
        [7, 5, 3],
        [2, 9, 4]
    ],
    [
        [4, 9, 2],
        [3, 5, 7],
        [8, 1, 6]
    ],
    [
        [2, 9, 4],
        [7, 5, 3],
        [6, 1, 8]
    ],
    [
        [8, 3, 4],
        [1, 5, 9],
        [6, 7, 2]
    ],
    [
        [4, 3, 8],
        [9, 5, 1],
        [2, 7, 6]
    ],
    [
        [6, 7, 2],
        [1, 5, 9],
        [8, 3, 4]
    ],
    [
        [2, 7, 6],
        [9, 5, 1],
        [4, 3, 8]
    ]
];

//https://www.hackerrank.com/challenges/magic-square-forming/problem
function formingMagicSquare(s) {
    let squareIndex = -1;
    let lessDiff = 45;
    magicSquares.forEach((square, index) => {
        let diff = 0;
        for (let i = 0; i < square.length; i++) {
            for (let j = 0; j < square[i].length; j++) {
                if (square[i][j] != s[i][j]) {
                    diff += Math.abs(square[i][j] - s[i][j]);
                    if (diff > lessDiff) {
                        break;
                    }
                }
            }
        }

        if (diff < lessDiff) {
            squareIndex = index;
            lessDiff = diff;
        }
    });

    console.log('square index => %d', squareIndex);

    return lessDiff;
}

console.log(formingMagicSquare([
    [2, 9, 8],
    [4, 2, 7],
    [5, 6, 7]
]));
