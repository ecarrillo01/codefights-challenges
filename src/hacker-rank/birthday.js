//https://www.hackerrank.com/challenges/the-birthday-bar/problem
//m squares summing d
function birthday(s, d, m) {
    let count = 0;
    let iMax = s.length;
    let iMin = m - 1;
    for (let i = iMin; i < iMax; i++) {
        let sum = 0;
        let min = i + 1 - m;
        for (let j = min; j <= i; j++) {
            sum += s[j];
            if (sum > d) {
                break;
            }
        }
        if (sum === d) {
            count++;
        }
    }

    return count;
}
const arr = [2, 5, 1, 3, 4, 4, 3, 5, 1, 1, 2, 1, 4, 1, 3, 3, 4, 2, 1];
console.log(birthday(arr, 18, 7));