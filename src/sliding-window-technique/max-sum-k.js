//https://www.geeksforgeeks.org/window-sliding-technique/
//Using sliding window technique
function maxSumOfElements(arr, windowSize) {
    let maxSum = 0;
    let currentSum = 0;
    for (let i = 0; i < windowSize; i++) {
        currentSum += arr[i];
        maxSum = currentSum;
    }
    for (let right = windowSize, left = 0; right < arr.length; right++) {
        currentSum += arr[right] - arr[left];
        maxSum = Math.max(maxSum, currentSum);
        left++;
    }
    return maxSum;
}

console.log(maxSumOfElements([100, 200, 300, 400], 2));//700
console.log(maxSumOfElements([1, 4, 2, 10, 23, 3, 1, 0, 20], 4));//39
console.log(maxSumOfElements([1, 4, 2, 10, 2, 3, 1, 0, 20], 4));

