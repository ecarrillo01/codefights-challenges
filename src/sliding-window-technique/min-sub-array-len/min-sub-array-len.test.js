const minSubArrayLen = require('./min-sub-array-len');

describe('Min sub array len test suite', () => {
    it.each([
        { a: [2, 3, 1, 2, 4, 3], b: 7, expected: 2 },
        { a: [2, 1, 6, 5, 4], b: 9, expected: 2 },
        { a: [3, 1, 7, 11, 2, 9, 8, 21, 62, 33, 19], b: 52, expected: 1 },
        { a: [1, 4, 16, 22, 5, 7, 8, 9, 10], b: 39, expected: 3 },
        { a: [1, 4, 16, 22, 5, 7, 8, 9, 10], b: 55, expected: 5 },
        { a: [4, 3, 3, 8, 1, 2, 3], b: 11, expected: 2 },
        { a: [1, 4, 16, 22, 5, 7, 8, 9, 10], b: 95, expected: 0 },
    ])('minSubArrayLen($a,$b) should be $expected', ({ a, b, expected }) => expect(minSubArrayLen(a, b)).toBe(expected));
});