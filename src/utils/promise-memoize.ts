import { LocalStorageService } from './storage-service';

interface IPMemoConstructorArgs {
  minutes?: number;
  hours?: number;
  days?: number;
}

export default class PromiseMemoize {
  private memoizeMilliseconds = 86400 * 1000;
  constructor({ minutes, hours, days }: IPMemoConstructorArgs = {}) {
    if (minutes) {
      this.memoizeMilliseconds = 60 * 1000 * minutes;
    } else if (hours) {
      this.memoizeMilliseconds = 3600 * 1000 * hours;
    } else if (days) {
      this.memoizeMilliseconds = 86400 * 1000 * days;
    }
  }

  memo<T extends (...args: any[]) => any>(func: T) {
    return async (...args: Parameters<T>): Promise<ReturnType<T>> => {
      const argsKey = JSON.stringify(args);
      try {
        if (!LocalStorageService.getItem(argsKey)) {
          const r = await func(...args);
          setImmediate(() =>
            LocalStorageService.setItem(
              argsKey,
              JSON.stringify(r),
              this.memoizeMilliseconds
            )
          );
          return r;
        } else {
          console.log(`Returning results from cache`);
          //Update promise result on every request
          setImmediate(async () => {
            LocalStorageService.setItem(
              argsKey,
              JSON.stringify(await func(...args)),
              this.memoizeMilliseconds
            );
          });
          return JSON.parse(LocalStorageService.getItem(argsKey));
        }
      } catch (e) {
        LocalStorageService.removeItem(argsKey);
        throw e;
      }
    };
  }
}
