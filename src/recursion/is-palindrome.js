// https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/quiz/425982#overview
// Recursive palindrome function

// Write a recursive function called isPalindrome which returns true if the string passed to it 
// is a palindrome(reads the same forward and backward).Otherwise it returns false.

function isPalindrome(str) {
    if (str.length === 1) return true;
    if (str.length === 2) return str[0] === str[1];
    return str[0] === str.slice(-1) && isPalindrome(str.slice(1, -1));
}

console.log(isPalindrome('awesome')); // false
console.log(isPalindrome('foobar')); // false
console.log(isPalindrome('tacocat')); // true
console.log(isPalindrome('amanaplanacanalpanama')); // true
console.log(isPalindrome('amanaplanacanalpandemonium')); // false
