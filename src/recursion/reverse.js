// https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/quiz/425976#overview

// Write a recursive function called reverse which accepts a string and returns a new string in reverse.

function reverse(str) {
    if (str === '') return '';
    return str.charAt(str.length - 1) + reverse(str.substring(0, str.length - 1));
}


console.log(reverse('Hello'))