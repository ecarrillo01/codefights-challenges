// https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/quiz/425986#overview
// Write a function called collectStrings which accepts an object and returns an array of all the values in the object that have a typeof string

function collectStrings(obj) {
    let arr = [];
    for (let item of Object.values(obj)) {
        if (typeof item === 'string') {
            arr.push(item);
        }
        else if (item !== undefined && item !== null && item.constructor == Object) {
            arr = arr.concat(collectStrings(item));
        }

    }
    return arr;
}


const obj = {
    stuff: "foo",
    data: {
        val: {
            thing: {
                info: "bar",
                moreInfo: {
                    evenMoreInfo: {
                        weMadeIt: "baz"
                    }
                }
            }
        }
    }
}

console.log(collectStrings(obj)) // ["foo", "bar", "baz"])

