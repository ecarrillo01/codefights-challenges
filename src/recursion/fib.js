// https://jfbarrios.com/3-formas-de-obtener-el-fibonacci-de-un-numero-en-javascript

function fib(n) {
    if (n <= 1) return n;
    let prev2 = 0;
    let prev1 = 1;
    let sum = 0;
    for (let i = 2; i <= n; i++) {
        sum = prev2 + prev1;
        prev2 = prev1;
        prev1 = sum;
    }
    return sum;
}

console.log(fib(4)) // 3
console.log(fib(10)) // 55
console.log(fib(28)) // 317811
console.log(fib(35)) // 9227465
