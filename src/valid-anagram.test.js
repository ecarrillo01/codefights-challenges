const validAnagram = require('./valid-anagram');
describe('Valid anagram test suite', () => {
    it.each([
        { a: 'aaz', b: 'zza', expected: false },
        { a: 'anagram', b: 'nagaram', expected: true },
        { a: '', b: '', expected: true },
    ])('validAnagram($a,$b) should be $expected', ({ a, b, expected }) => expect(validAnagram(a, b)).toBe(expected));
});