// Write a function called isSubsequence which takes in two strings and checks whether the characters in the first string 
// form a subsequence of the characters in the second string.In other words, the function should check whether the characters in
// the first string appear somewhere in the second string, without their order changing.

function isSubsequence(s, t) {
    var i = 0;
    var j = 0;
    if (!s) return true;
    while (j < t.length) {
        if (t[j] === s[i]) i++;
        if (i === s.length) return true;
        j++;
    }
    return false;
}

module.exports = isSubsequence;

console.log(isSubsequence('hello', 'hello world')); // true
console.log(isSubsequence('sing', 'sting')); // true
console.log(isSubsequence('abc', 'abracadabra')); // true
console.log(isSubsequence('abc', 'acb')); // false (order matters)