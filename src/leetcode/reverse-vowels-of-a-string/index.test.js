const reverseVowels = require('./index');
describe('Reverse vowels of a string unit tests', () => {
    it.each([
        { a: 'IceCreAm', expected: 'AceCreIm' },
        { a: 'leetcode', expected: 'leotcede' },
    ])('Reverse vowels of a string $a should be $expected', ({ a, expected }) => expect(reverseVowels(a)).toBe(expected));

})