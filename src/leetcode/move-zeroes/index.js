/**
 * https://leetcode.com/problems/move-zeroes/description/?envType=study-plan-v2&envId=leetcode-75
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
    const max = nums.length;
    let left = 0;
    let right = 1;
    while (right < max) {
        while (nums[left] === 0 && nums[right] === 0) {
            right++;
        }
        if (right >= max) break;
        if (nums[left] === 0) {
            nums[left] = nums[right];
            nums[right] = 0;
        }
        left++;
        right++;
    }
    return nums;
};

module.exports = moveZeroes;