/**
 * https://leetcode.com/problems/product-of-array-except-self/?envType=study-plan-v2&envId=leetcode-75
 * @param {number[]} nums
 * @return {number[]}
 */
const productExceptSelf = function (arr) {
    if (arr.length === 1) return;
    const res = [];
    let prefix = 1;
    let suffix = 1
    for (let i = 0; i < arr.length; i++) {
        res[i] = prefix;
        prefix *= arr[i];
    }
    for (let i = arr.length - 1; i >= 0; i--) {
        res[i] *= suffix;
        suffix *= arr[i];
    }
    return res;
};

module.exports = productExceptSelf;