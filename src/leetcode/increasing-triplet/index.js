/**
 * https://leetcode.com/problems/increasing-triplet-subsequence/description/?envType=study-plan-v2&envId=leetcode-75
 * @param {number[]} nums
 * @return {boolean}
 */
const increasingTriplet = function (nums) {
    let min1 = Infinity;
    let min2 = Infinity;
    for (let item of nums) {
        if (item <= min1) {
            min1 = item;
        }
        else if (item <= min2) {
            min2 = item;
        }
        else {
            return true;
        }
    }
    return false;
};

module.exports = increasingTriplet;