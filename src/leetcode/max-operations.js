/**
 * https://leetcode.com/problems/max-number-of-k-sum-pairs/?envType=study-plan-v2&envId=leetcode-75
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var maxOperations = function (nums, k) {
    const countMap = new Map();
    let operations = 0;

    for (const num of nums) {
        const complement = k - num;
        if (countMap.has(complement) && countMap.get(complement) > 0) {
            // Found a pair
            operations += 1;
            countMap.set(complement, countMap.get(complement) - 1);
        } else {
            // Add current number to the map
            countMap.set(num, (countMap.get(num) || 0) + 1);
        }
    }

    return operations;
};

console.log(maxOperations([3, 1, 3, 4, 3], 6));

//console.log(maxOperations([4, 4, 1, 3, 1, 3, 2, 2, 5, 5, 1, 5, 2, 1, 2, 3, 5, 4], 2)); // 2