const maxArea = require('./index');

describe('Container with most water suite', () => {
    it.each([
        {
            input: [1, 8, 6, 2, 5, 4, 8, 3, 7], expected: 49,
        },
    ])('Container with max area', ({ input, expected }) => expect(maxArea(input)).toEqual(expected));
});



