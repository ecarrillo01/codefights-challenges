const compress = require('./index');

describe('Compress string test suite', () => {
    it.each([
        {
            input: ['a', 'a', 'b', 'b', 'c', 'c', 'c'], modifiedInput: ['a', '2', 'b', '2', 'c', '3', 'c'], expected: 6
        },
        {
            input: ['a'], modifiedInput: ['a'], expected: 1
        },
        {
            input: ['a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'], modifiedInput: ['a', 'b', '1', '2', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'], expected: 4
        },
        {
            input: ['a', 'a', 'a', 'b', 'b', 'a', 'a'], modifiedInput: ['a', '3', 'b', '2', 'a', '2', 'a'], expected: 6
        },
    ])('Compress string %s', ({ input, modifiedInput, expected }) => {
        expect(compress(input)).toBe(expected)
        expect(input).toEqual(modifiedInput);
    });
});



