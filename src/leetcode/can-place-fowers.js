// https://leetcode.com/problems/can-place-flowers/?envType=study-plan-v2&envId=leetcode-75

/**
 * @param {number[]} flowerbed
 * @param {number} n
 * @return {boolean}
 */
const canPlaceFlowers = function (flowerbed, n) {
    let count = 0;
    if (n > flowerbed.length) return false;
    for (let i = 0; i < flowerbed.length; i++) {
        // Check if the current plot is empty.
        if (flowerbed[i] == 0) {
            const emptyLeftPlot = i === 0 || flowerbed[i - 1] === 0;
            const emptyRightPlot = i === flowerbed.length - 1 || flowerbed[i + 1] == 0;
            // If both plots are empty, we can plant a flower here.
            if (emptyLeftPlot && emptyRightPlot) {
                flowerbed[i] = 1;
                count++;
            }
        }

    }
    return count >= n;
};