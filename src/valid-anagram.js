// https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/learn/lecture/9816156#overview
function validAnagram(str1, str2) {
    if (str1.length !== str2.length) return false;
    const lookup = {};
    for (let letter of str1) {
        lookup[letter] = ++lookup[letter] || 1;
    }

    for (let letter of str2) {
        // can't find letter or letter is zero then it's not an anagram
        if (!lookup[letter]) return false;
        lookup[letter]--;
    }

    return true;

}

module.exports = validAnagram;