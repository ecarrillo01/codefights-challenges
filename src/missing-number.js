//https://practice.geeksforgeeks.org/problems/missing-number-in-array1416/1?utm_source=geeksforgeeks&utm_medium=article_practice_tab&utm_campaign=article_practice_tab
function missingNumber(array, n) {
    //formula to calculate the sum of consecutive numbers from 1 to n
    const totalSum = n * (n + 1) / 2;
    let res = totalSum;
    for (let i = 0, max = array.length; i < max; i++) {
        res -= array[i];
    }
    return res;
}

console.log(missingNumber([1, 2, 4, 6, 3, 7, 8], 7));
console.log(missingNumber([1, 2, 3, 5], 5));