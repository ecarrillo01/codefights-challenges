const countUniqueValues = require("./count-unique-values");

describe('Count unique values tests', () => {
    it('Should be 2', () => {
        const actual = countUniqueValues([1, 1, 1, 1, 1, 2]);
        expect(actual).toBe(2);
    });

    it('Should be 7', () => {
        const actual = countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]);
        expect(actual).toBe(7);
    });
});