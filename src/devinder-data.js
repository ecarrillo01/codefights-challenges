const input = {
    "9783736411050": ["Leo Tolstoy", "War and Peace", "Well, Prince, Genoa and Lucca are now no more than private estates of the Bonaparte family", "No, I warn you, that if you do not tell me we are at war, if you again allow yourself to palliate all the infamies and atrocities of this Antichrist (upon my word, I believe he is), I don't know you in future, you are no longer my friend, no longer my faithful slave, as you say"],
    "9781455883493": ["Neal Stephenson", "Diamond Age", "The bells of St. Mark's were ringing changes up on the mountain when Bud skated over to the mod parlor to upgrade his skull gun", "Bud's relationship with the female sex was governed by a gallimaufry of primal impulses, dim suppositions, deranged theories, overheard scraps of conversation, half-remembered pieces of bad advice, and fragments of no-doubt exaggerated anecdotes that amounted to rank superstition"],
    "9780736639859": ["Leo Tolstoy", "Anna Karenina", "Happy families are all alike; every unhappy family is unhappy in its own way", "Everything was in confusion in the Oblonskys' house"],
    "9788491051329": ["Jane Austen", "Pride and Prejudice", "It is a truth universally acknowledged, that a single man in possession of a good fortune must be in want of a wife", "However little known the feelings or views of such a man may be on his first entering a neighbourhood, this truth is so well fixed in the minds of the surrounding families, that he is considered as the rightful property of some one or other of their daughters"],
    "9780140232929": ["Neal Stephenson", "Snow Crash", "The Deliverator belongs to an elite order, a hallowed subcategory", "He's got esprit up to here"],
    "9788842516651": ["Alexandre Dumas", "The Three Musketeers", "On the first Monday of the month of April, 1625, the market town of Meung, in which the author of ROMANCE OF THE ROSE was born, appeared to be in as perfect a state of revolution as if the Huguenots had just made a second La Rochelle of it", "Many citizens, seeing the women flying toward the High Street, leaving their children crying at the open doors, hastened to don the cuirass, and supporting their somewhat uncertain courage with a musket or a partisan, directed their steps toward the hostelry of the Jolly Miller, before which was gathered, increasing every minute, a compact group, vociferous and full of curiosity"]
}

// Fast access to autors
const authorsDict = {};

for (const isbn in input) {
    const author = input[isbn].at(0).split(' ').reverse().join(', ');
    const title = input[isbn].at(1);
    const text = input[isbn].slice(2);
    const book = {
        title: title,
        isbn: isbn,
        text: text
    };

    if (authorsDict[author]) {
        authorsDict[author].books.push(book);
    } else {
        authorsDict[author] = {
            name: author,
            books: [book]
        };
    }

}


const sortedData = Object.values(authorsDict).sort((a, b) => a.name < b.name ? -1 : 1);

for (let item of sortedData) {
    item.books.sort((a, b) => {
        if (a.title < b.title) {
            return -1;
        } else {
            return 1;
        }
    })
}

console.log(JSON.stringify(sortedData))