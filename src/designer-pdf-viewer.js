function genCharArray(charA, charZ) {
    var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
        a.push(String.fromCharCode(i));
    }
    return a;
}

function designerPdfViewer(h, word) {
    let alphabetLetters = genCharArray('a', 'z');
    let wordArr = word.split('');
    let higher = 0;
    for (let letter of wordArr) {
        const letterIndex = alphabetLetters.indexOf(letter);
        if (higher === 0) {
            higher = h[letterIndex];
        }
        else {
            if (h[letterIndex] > higher) {
                higher = h[letterIndex];
            }
        }
    }
    return higher * wordArr.length;
}



let h = [1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
designerPdfViewer(h, 'abc');
