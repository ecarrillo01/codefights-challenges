const SinglyLinkedList = require('./single-linked-list');

describe('Single linked list test suite', () => {
    let list;
    beforeEach(() => {
        list = new SinglyLinkedList();
        list.push("HELLO");
        list.push("GOODBYE");
        list.push("!");
        list.unshift('bla');
    });

    it('List size decreased at removed element from specific index', () => {
        list.remove(3);
        expect(list.length).toBe(3);
    });

    it('Remove last index', () => {
        list.remove(list.length - 1);
        expect(list.length).toBe(3);
    });

    it('Remove first index', () => {
        list.remove(0);
        expect(list.length).toBe(3);
    });

    it('Try remove item from index less than 0', () => {
        const actual = list.remove(-1);
        expect(actual).toBeUndefined();
    });

    it('Try remove item from index greater than list length', () => {
        const actual = list.remove(1000);
        expect(actual).toBeUndefined();
    });

    it('Should insert item at the end of the list', () => {
        let curLen = list.length;
        list.push(100);
        expect(list.tail.val).toBe(100);
        expect(list.length).toBe(curLen + 1);
        expect(list.get(list.length - 1).val).toBe(100);
    });

    it('Should remove last element', () => {
        let curLen = list.length;
        list.push(100);
        list.pop();
        expect(list.length).toBe(curLen);
    });

});