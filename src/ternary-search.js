function ternarySearch(arr, x){
    let l = 0;
    let r = arr.length - 1;
    let mid1, mid2;
    while (r >= l) {
        mid1 = l + Math.floor(((r-l)/3));
        mid2 = r - Math.floor(((r-l)/3));

        // Check if key is present at any mid
        if(x === arr[mid1]){
            return mid1;
        } else if(x === arr[mid2]){
            return mid2;
        } 
        
        // Since key is not present at mid,
        // check in which region it is present
        // then repeat the Search operation
        // in that region
        if(x < arr[mid1]){
            // The key lies in between l and mid1
            r = mid1 - 1;
        } else if(x > arr[mid2]){
            // The key lies in between mid2 and r
            l = mid2 + 1;
        } else {
            // The key lies in between mid1 and mid2
            l = mid1 + 1;
            r = mid2 - 1;
        }
   }
 
   // We reach here when element is not
   // present in array
   return -1;
}

console.log('ternarySearch:', ternarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9], 9));